`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:36:51 02/01/2017 
// Design Name: 
// Module Name:    cnt_up 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cnt #(parameter MSB=8)
(
    input                CLK,
    input                EN,
	 input                DWN,
	 input                CMP,
	 input      [MSB-1:0] CMP_DATA,
	 output               INTR,
    output reg [MSB-1:0] DATA = 0
    );

wire reset_cmp;
assign reset_cmp = (CMP == 1 && EN == 1 && DATA == CMP_DATA) ? 1'b1 : 1'b0;
assign INTR =((DATA == 0 && EN == 1 && CMP == 0) || reset_cmp ) ? 1'b1 : 1'b0;

always@(posedge CLK) 
	begin
	if(EN) 
		begin
		if(CMP && INTR) DATA <= 0;
		else if(DWN) DATA <= DATA - 1'b1;
		else DATA <= DATA + 1'b1;
		end
	else DATA <= 0;
	end

endmodule
