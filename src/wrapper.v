`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:36:08 02/01/2017 
// Design Name: 
// Module Name:    wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module wrapper(
    input            CLK,
    output reg [7:0] LED = 8'h00,
    input      [4:0] BUTTONS,
    input      [7:0] SWITCH
    );
	 
reg        cnt_en = 0;
reg [7:0]  cnt_data = 0;

wire button_press;
assign button_press = BUTTONS[0] || BUTTONS[1] || BUTTONS[2] || BUTTONS[3] || BUTTONS[4];
wire shift_en;

cnt#(24) delay 
(
	.CLK  (CLK), 
   .EN   (1'b1), 
   .DWN  (1'b0),
	.CMP  (1'b1),
   .INTR (shift_en),
	.CMP_DATA ({SWITCH, 16'hFFFF})
);

always@(posedge CLK) begin
	if(shift_en) 
		begin
		if(!button_press) 
			begin
			if(LED[7] != 1'b1) LED <= {LED[0+:7], 1'b0};
			else LED <= 8'h01;
			end
		else
			begin
			if(LED[0] != 1'b1) LED <= {1'b0, LED[1+:7]};
			else LED <= 8'h80;
			end
		end
	else
		if(LED == 0) LED <= 8'h01;
end
	
endmodule
