`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:26:20 02/01/2017
// Design Name:   wrapper
// Module Name:   /home/xilinx/Public/testAtlys/wrapper_tb.v
// Project Name:  testAtlys
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: wrapper
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module wrapper_tb;

	// Inputs
	reg CLK;
	reg [4:0] BUTTONS;
	reg [7:0] SWITCH;

	// Outputs
	wire [7:0] LED;

	// Instantiate the Unit Under Test (UUT)
	wrapper uut (
		.CLK(CLK), 
		.LED(LED), 
		.BUTTONS(BUTTONS), 
		.SWITCH(SWITCH)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		BUTTONS = 0;
		SWITCH = 0;

		// Wait 100 ns for global reset to finish
		#100;
      BUTTONS[0] = 1'b1;
		#100;
		BUTTONS[0] = 1'b0;
		SWITCH = 8'h01;
		// Add stimulus here

	end
   always#1 CLK = ~CLK; 
endmodule

